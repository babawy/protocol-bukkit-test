package babawy.protocol.handler;


import net.nathem.websocket.NathemWSC;
import net.nathem.websocket.NathemWSHandler;
import org.bukkit.Bukkit;
import org.json.simple.JSONObject;

public class ServerInfoHandler extends NathemWSHandler{

    public ServerInfoHandler(NathemWSC client, String uuid, String type, JSONObject data) {
        super(client, uuid, type, data);
    }

    @Override
    protected JSONObject handle(JSONObject jsonObject) {
        JSONObject infos = new JSONObject();
        infos.put("version", Bukkit.getVersion());
        infos.put("server-name", Bukkit.getServerName());
        infos.put("online-players", Bukkit.getOnlinePlayers().size());
        return infos;
    }

    @Override
    protected void onResponseSent() {

    }
}
