package babawy.protocol.request;


import net.nathem.websocket.NathemWSRequest;
import org.bukkit.entity.Player;
import org.json.simple.JSONObject;

public class PlayerInfoRequest extends NathemWSRequest{

    private Player player;

    public PlayerInfoRequest(Player player) {
        this.player = player;
    }

    public Player getPlayer() {
        return player;
    }

    @Override
    public String getType() {
        return "PLAYER_INFOS";
    }

    @Override
    public JSONObject buildData() {
        JSONObject data = new JSONObject();
        data.put("player-uuid", this.player.getUniqueId().toString());
        data.put("player-name", this.player.getName());
        return data;
    }

    @Override
    public void onResponse(JSONObject jsonObject) {
        // Update player data here
    }
}
