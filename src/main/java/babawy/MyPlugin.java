package babawy;

import babawy.protocol.handler.ServerInfoHandler;
import babawy.protocol.request.PlayerInfoRequest;
import net.nathem.websocket.NathemWSC;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class MyPlugin extends JavaPlugin{

    private NathemWSC protocol;

    @Override
    public void onEnable() {
        super.onEnable();

        Bukkit.getLogger().info("Test plugin successfully loaded !");

        // Starting protocol
        this.protocol = new NathemWSC("minecraft_server", "localhost:1234", "myprivatekey");

        // Registering handlers
        this.protocol.registerHandler("SERVER_INFOS", ServerInfoHandler.class);

        // Sending requests...
        for(Player player : Bukkit.getOnlinePlayers()){
            this.protocol.sendRequest(new PlayerInfoRequest(player));
        }
    }
}
